# Install

Make sure your Python version is at least: 3.6

#### Install as Python package
- Install packages: pip, setuptools
- Install dependencies: pip install -r requirements.cfg
- Build package: python setup.py build
- Install package: python setup.py install
