#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
import platform
import ctypes


class App(wx.App):

    def __init__(
        self,
        redirect=False,
        filename=None,
        useBestVisual=False,
        clearSigInt=True
    ):
        osName = platform.system()
        if osName == "Linux":
            x11 = ctypes.cdll.LoadLibrary("libX11.so")
            x11.XInitThreads()
        elif osName == "Windows":
            pass
        elif osName == "Darwin":
            pass

        wx.App.__init__(
            self,
            redirect=redirect,
            filename=filename,
            useBestVisual=useBestVisual,
            clearSigInt=clearSigInt
        )
