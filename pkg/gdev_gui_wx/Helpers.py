#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx


def applyPanelOnEmptyPanel(parent, child):
    sizer = wx.BoxSizer(wx.VERTICAL)
    parent.SetSizer(sizer)
    sizer.Add(child, 1, wx.EXPAND | wx.ALL, 0)
