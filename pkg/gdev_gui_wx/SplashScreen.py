#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx.lib.agw.advancedsplash


class SplashScreen(wx.lib.agw.advancedsplash.AdvancedSplash):

    def __init__(self, wnd, resourceProvider, imagePath, timeout=1200):
        bmp = wx.Bitmap(
            resourceProvider.getResourcePath(imagePath),
            wx.BITMAP_TYPE_PNG)
        img = bmp.ConvertToImage()
        img.ConvertAlphaToMask()
        bmp = wx.Bitmap(img)
        wx.lib.agw.advancedsplash.AdvancedSplash.__init__(
            self,
            parent=None,
            id=wx.ID_ANY,
            pos=wx.DefaultPosition,
            size=wx.DefaultSize,
            style=wx.FRAME_SHAPED | wx.NO_BORDER | wx.STAY_ON_TOP,
            bitmap=bmp,
            timeout=timeout,
            agwStyle=wx.lib.agw.advancedsplash.AS_CENTER_ON_SCREEN | wx.lib.agw.advancedsplash.AS_TIMEOUT
        )
        self._wnd = wnd

        # Bind events:
        self.Bind(wx.EVT_CLOSE, self.__onClose)
        self.Bind(wx.EVT_TIMER, self.__onNotify)

    def __onClose(self, event):
        if not self._wnd.IsShown():
            self._wnd.Show()
        self.Destroy()

    def __onNotify(self, event):
        self._wnd.Show()
        self.Close(True)
