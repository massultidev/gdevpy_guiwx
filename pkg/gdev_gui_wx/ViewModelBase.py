#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

from typing import *
from abc import ABC, abstractmethod


class ViewModelBase(ABC):

    def __init__(
        self,
        parent: Any = None,
        model: Any = None
    ):
        self._model = model
        self._view = self._createView(parent)

        if self._model is not None:
            self._loadModel()

    @property
    def view(self) -> Any:
        return self._view

    @property
    def model(self) -> Any:
        return self._model

    @model.setter
    def model(self, value: Any) -> None:
        if value is None:
            raise RuntimeError('Model cannot be set to None!')
        self._model = value
        self._loadModel()

    def refresh(self) -> None:
        if self._model is None:
            raise RuntimeError('There is no model to load!')
        self._loadModel()

    @abstractmethod
    def _createView(self, parent: Any) -> None:
        """Should return a complete view."""
        pass

    @abstractmethod
    def _loadModel(self) -> None:
        """Should load model data into view."""
        pass
