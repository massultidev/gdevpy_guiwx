#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import wx
import wx.xrc

from gdev_gui_wx.Helpers import applyPanelOnEmptyPanel


def _defineXrcView(ViewBase, loadViewMethod):
    class XrcView(ViewBase):

        def __init__(self, parent, resourceProvider, viewName):
            super().__init__()

            xrcFilePath = resourceProvider.getResourcePath('view/{}.xrc'.format(viewName))
            xmlResource = wx.xrc.XmlResource(xrcFilePath)
            if not loadViewMethod(xmlResource, self, parent=parent, name=viewName):
                raise RuntimeError('Loading view "{}" failed!'.format(viewName))

        def __getattr__(self, name):
            return wx.xrc.XRCCTRL(self, name)

        def getId(self, controlName):
            return wx.xrc.XRCID(controlName)

    return XrcView


XrcFrameView = _defineXrcView(wx.Frame, wx.xrc.XmlResource.LoadFrame)
XrcDialogView = _defineXrcView(wx.Dialog, wx.xrc.XmlResource.LoadDialog)


class XrcPanelView(wx.Panel):

    def __init__(self, parent, resourceProvider, viewName):
        super().__init__(parent=parent)

        xrcFilePath = resourceProvider.getResourcePath('view/{}.xrc'.format(viewName))
        xmlResource = wx.xrc.XmlResource(xrcFilePath)
        self._xrcPanel = xmlResource.LoadPanel(parent=self, name=viewName)

        applyPanelOnEmptyPanel(self, self._xrcPanel)

    def __getattr__(self, name):
        return wx.xrc.XRCCTRL(self._xrcPanel, name)
